""" Solution to Project Euler problem #54

Link: https://projecteuler.net/problem=54"""

from collections import Counter
from enum import Enum
import itertools
from typing import List, Tuple


class Rank(Enum):
    """The twelve ranks in a standard playing card deck"""

    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13
    ACE = 14


ROYAL_SET = set(
    [Rank.TEN.value, Rank.JACK.value, Rank.QUEEN.value, Rank.KING.value, Rank.ACE.value]
)


class Suit(Enum):
    """The four suits in a standard playing card deck"""

    HEARTS = "H"
    SPADES = "S"
    CLUBS = "C"
    DIAMONDS = "D"


class HandType(Enum):
    """The various types of poker hands combinations,
    ranked in ascending order of strength"""

    HIGH_CARD = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE_OF_KIND = 3
    STRAIGHT = 4
    FLUSH = 5
    FULL_HOUSE = 6
    FOUR_OF_A_KIND = 7
    STRAIGHT_FLUSH = 8
    ROYAL_FLUSH = 9


# Hands which need to be checked for high values in ties
HIGH_CHECKS = [
    HandType.ONE_PAIR,
    HandType.TWO_PAIR,
    HandType.THREE_OF_KIND,
    HandType.FULL_HOUSE,
    HandType.FOUR_OF_A_KIND,
]


class Card:
    """A card has a rank and a suit"""

    rank = None
    suit = None

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    @property
    def as_string(self):
        rank = self.rank.value
        if self.rank == Rank.ACE:
            rank = "A"
        if self.rank == Rank.TEN:
            rank = "T"
        if self.rank == Rank.JACK:
            rank = "J"
        if self.rank == Rank.QUEEN:
            rank = "Q"
        if self.rank == Rank.KING:
            rank = "K"

        return str(rank) + self.suit.value


def parse_hand_string(hand_str: str) -> List[Card]:
    """Takes a string of five cards, and parses them into Card objects and
    returns a list of all five cards

    hand_str example: 5C 4D 3C JD KC"""
    cards = []
    card_strings = hand_str.split(" ")
    if len(card_strings) != 5:
        raise ValueError("Hand does not have exactly five cards in it")
    for card_string in card_strings:
        # Parse out our rank and suit for this card
        rank = card_string[0]

        if rank == "T":
            rank = 10
        if rank == "J":
            rank = 11
        if rank == "Q":
            rank = 12
        if rank == "K":
            rank = 13
        if rank == "A":
            rank = 14

        rank = int(rank)
        suit = card_string[1]
        cards.append(Card(rank=Rank(rank), suit=Suit(suit)))
    return cards


class Hand:
    """A hand has five cards in it"""

    __cache__ = {}
    cards = []

    def __init__(self, hand_str: str):
        # Reset our cache when we rebuild the hand
        self.__cache__ = {}
        # Parse the incoming string into Card instances
        self.cards = parse_hand_string(hand_str)

    @property
    def suits(self):
        """Return a list of all suits in hand"""
        if "suit_list" not in self.__cache__:
            self.__cache__["suit_list"] = [card.suit.value for card in self.cards]
        return self.__cache__["suit_list"]

    @property
    def ranks(self) -> List[int]:
        """Return a list of all ranks in hand, sorted by value"""
        if "rank_list" not in self.__cache__:
            self.__cache__["rank_list"] = sorted(
                [card.rank.value for card in self.cards]
            )
        return self.__cache__["rank_list"]

    def high_cards(self) -> List[Card]:
        """Returns list of cards not in pairs or runs, sorted by highest value"""
        # create sets out of our runs, so we can remove them from our high cards
        card_set = set(self.cards)
        pair_set = set(list(itertools.chain(*self.pairs())))
        three_set = set(self.three_of_a_kind())
        four_set = set(self.four_of_a_kind())
        return sorted(
            list(card_set - pair_set - three_set - four_set),
            key=lambda x: x.rank.value,
            reverse=True,
        )

    def pairs(self) -> List[List[Card]]:
        """Returns all pairs of card ranks"""
        if "pairs" not in self.__cache__:
            self.__cache__["pairs"] = []
            cards = sorted(self.cards, key=lambda x: x.rank.value)
            j = 0
            for i in range(0, len(self.cards)):
                try:
                    if cards[j].rank.value == cards[j + 1].rank.value:
                        self.__cache__["pairs"].append([cards[j], cards[j + 1]])
                        # skip j + 1 card as we've just matched it
                        j += 1
                except IndexError:
                    # we've no more cards to compare
                    pass
                j += 1
        return self.__cache__["pairs"]

    def three_of_a_kind(self) -> List[Card]:
        """Returns list of any found three of a kind"""
        if "three_of_a_kind" not in self.__cache__:
            self.__cache__["three_of_a_kind"] = []
            # Scan our hand, sorted by rank, for runs of three cards with same rank
            cards = sorted(self.cards, key=lambda x: x.rank.value)
            for i in range(0, len(self.cards)):
                try:
                    if cards[i].rank == cards[i + 1].rank == cards[i + 2].rank:
                        self.__cache__["three_of_a_kind"] = [
                            cards[i],
                            cards[i + 1],
                            cards[i + 2],
                        ]
                except IndexError:
                    # we ran out of cards, no more threes, for sure
                    pass
        return self.__cache__["three_of_a_kind"]

    def four_of_a_kind(self) -> List[Card]:
        """Returns list of any found four of a kind"""
        if "four_of_a_kind" not in self.__cache__:
            self.__cache__["four_of_a_kind"] = []
            # Scan our hand, sorted by rank, for runs of four cards with same rank
            cards = sorted(self.cards, key=lambda x: x.rank.value)
            for i in range(0, len(self.cards)):
                try:
                    if (
                        cards[i].rank
                        == cards[i + 1].rank
                        == cards[i + 2].rank
                        == cards[i + 3].rank
                    ):
                        self.__cache__["four_of_a_kind"] = [
                            cards[i],
                            cards[i + 1],
                            cards[i + 2],
                            cards[i + 3],
                        ]
                except IndexError:
                    # we ran out of cards, no more four, for sure
                    pass
        return self.__cache__["four_of_a_kind"]

    @property
    def has_one_pair(self) -> bool:
        """Returns whether the hand has at least one pair"""
        if len(self.pairs()) >= 1:
            return True
        return False

    @property
    def has_two_pairs(self) -> bool:
        """Returns whether the hand has at two pairs"""
        if len(self.pairs()) >= 2:
            return True
        return False

    @property
    def has_three_of_a_kind(self) -> bool:
        """Returns whether the hand has three of a kind"""
        if self.three_of_a_kind():
            return True
        return False

    @property
    def has_four_of_a_kind(self) -> bool:
        """Returns whether the hand has four of a kind"""
        if self.four_of_a_kind():
            return True
        return False

    @property
    def has_straight(self) -> bool:
        """Returns whether the hand has all consecutive values"""
        if "straight" not in self.__cache__:
            self.__cache__["straight"] = False
            # check for special case of low ace straight
            if sorted(self.ranks) == [2, 3, 4, 5, 14]:
                self.__cache__["straight"] = True
                return self.__cache__["straight"]
            if sorted(self.ranks) == list(range(min(self.ranks), max(self.ranks) + 1)):
                self.__cache__["straight"] = True
        return self.__cache__["straight"]

    @property
    def has_flush(self) -> bool:
        """Returns whether the hand is all of the same suit"""
        if 5 in Counter(self.suits).values():
            return True
        return False

    @property
    def has_full_house(self) -> bool:
        """Returns whether the hand has three of a kind, and a pair"""
        # if we have a three of a kind, and a pair ...
        if self.has_three_of_a_kind and self.has_one_pair:
            # ... and at least one found pair is not in the three of a kind ...
            for pair in self.pairs():
                if not set(pair).issubset(set(self.three_of_a_kind())):
                    # ... we've got a full house
                    return True
        return False

    @property
    def has_straight_flush(self) -> bool:
        """Returns whether the hand is all of the same suit in consecutive rank"""
        if self.has_straight and self.has_flush:
            return True
        return False

    @property
    def has_royal_flush(self) -> bool:
        """Returns whether the hand is all of the same suit in consecutive rank"""
        if self.has_flush and ROYAL_SET == set(self.ranks):
            return True
        return False


def compare_hands(p1_hand: Hand, p2_hand: Hand) -> Tuple[int, HandType, Hand]:
    """Given two Hand objects, determine which one "wins" based on
    standard poker criteria.

    Note: This function is overly complex and many parts of
    the logic can be broken into reusable pieces, but time
    grows short.

    Returns integer representing whether hand 1 or 2 won"""
    # Calculate each player's best combination
    player_one_best = best_hand(p1_hand)
    player_two_best = best_hand(p2_hand)

    if player_one_best.value > player_two_best.value:
        return 1, player_one_best, p1_hand

    if player_two_best.value > player_one_best.value:
        return 2, player_two_best, p2_hand

    if player_one_best.value == player_two_best.value:
        hand_type = player_one_best
        if hand_type in HIGH_CHECKS:
            if hand_type == HandType.FOUR_OF_A_KIND:
                p1_value = p1_hand.four_of_a_kind()[0].rank.value
                p2_value = p2_hand.four_of_a_kind()[0].rank.value
                if p1_value > p2_highvalue:
                    return 1, hand_type, p1_hand

                if p2_value > p1_value:
                    return 2, hand_type, p2_hand

            if hand_type in [HandType.FULL_HOUSE, HandType.THREE_OF_KIND]:
                p1_value = p1_hand.three_of_a_kind()[0].rank.value
                p2_value = p2_hand.three_of_a_kind()[0].rank.value
                if p1_value > p2_value:
                    return 1, hand_type, p1_hand

                if p2_value > p1_value:
                    return 2, hand_type, p2_hand

            # Check pair high values
            p1_pair_value = p1_hand.pairs()[0][0].rank.value
            p2_pair_value = p2_hand.pairs()[0][0].rank.value

            if p1_pair_value > p2_pair_value:
                return 1, hand_type, p1_hand

            if p2_pair_value > p1_pair_value:
                return 2, hand_type, p2_hand

        p1_high = p1_hand.high_cards()
        p2_high = p2_hand.high_cards()

        for i in range(0, 5):
            if p1_high[i].rank.value > p2_high[i].rank.value:
                return 1, hand_type, p1_hand
            if p2_high[i].rank.value > p1_high[i].rank.value:
                return 2, hand_type, p2_hand
    return


def best_hand(hand: Hand) -> HandType:
    """Given a Hand"""
    if hand.has_royal_flush:
        return HandType.ROYAL_FLUSH
    if hand.has_straight_flush:
        return HandType.STRAIGHT_FLUSH
    if hand.has_four_of_a_kind:
        return HandType.FOUR_OF_A_KIND
    if hand.has_full_house:
        return HandType.FULL_HOUSE
    if hand.has_flush:
        return HandType.FLUSH
    if hand.has_straight:
        return HandType.STRAIGHT
    if hand.has_three_of_a_kind:
        return HandType.THREE_OF_KIND
    if hand.has_two_pairs:
        return HandType.TWO_PAIR
    if hand.has_one_pair:
        return HandType.ONE_PAIR
    return HandType.HIGH_CARD


if __name__ == "__main__":
    player_one_wins = 0
    with open("poker.txt") as pokerFile:
        for deal in pokerFile:
            # FIXME Should we just take lists of card strings?
            deal_list = deal.split(" ")
            p1_hand = Hand(" ".join(deal_list[:5]))
            p2_hand = Hand(" ".join(deal_list[5:]))
            w, ht, h = compare_hands(p1_hand, p2_hand)
            if w == 1:
                player_one_wins += 1
    print("Player one wins {0} times".format(player_one_wins))
