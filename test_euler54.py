""" Tests for euler_54 classes and functions"""

import unittest
from euler54 import Hand, compare_hands, HandType


class TestHand(unittest.TestCase):
    """Test the interface of our Hand class"""

    def setUp(self):
        """Setup our sample data"""
        self.valid_hand_str = "5C 2H 3C JD KC"
        self.valid_hand_high_card = "KC"
        self.invalid_hand_str = "5C 2H 3C JD"

    def test_create_valid_hand(self):
        self.assertEqual(5, len(Hand(self.valid_hand_str).cards))

    def test_create_invalid_hand_throws_valueerror(self):
        with self.assertRaises(ValueError) as context:
            Hand(self.invalid_hand_str)
            self.assertTrue(
                "Hand does not have exactly five cards in it" in str(context.exception)
            )

    def test_single_pair(self):
        single_pair_str = "4C 4H 5H 3D AS"
        self.assertEqual(len(Hand(single_pair_str).pairs()), 1)

    def test_single_pair_with_one_extra(self):
        single_pair_str = "4C 4H 4C 3D AS"
        self.assertEqual(len(Hand(single_pair_str).pairs()), 1)

    def test_two_pairs(self):
        two_pair_str = "4S 4H 4D 4C AS"
        self.assertEqual(len(Hand(two_pair_str).pairs()), 2)

    def test_has_one_pair(self):
        single_pair_str = "4S 4H 3D 4C AS"
        self.assertTrue(Hand(single_pair_str).has_one_pair)

        no_single_pair_str = "4S 9H 3D KC AS"
        self.assertFalse(Hand(no_single_pair_str).has_one_pair)

    def test_has_two_pairs(self):
        two_pair_str = "4S 4H 9D 9C AS"
        self.assertTrue(Hand(two_pair_str).has_two_pairs)

        no_two_pair_str = "4S 4H 9D 8C AS"
        self.assertFalse(Hand(no_two_pair_str).has_two_pairs)

    def test_has_three_of_a_kind(self):
        three_of_a_kind_str = "AC AS 9D 4D AD"
        self.assertTrue(Hand(three_of_a_kind_str).has_three_of_a_kind)

    def test_has_straight(self):
        low_ace_straight_str = "AC 2D 5C 4S 3D"
        self.assertTrue(Hand(low_ace_straight_str).has_straight)

        straight_str = "7C 6D 8C 9S TD"
        self.assertTrue(Hand(straight_str).has_straight)

    def test_has_flush(self):
        flush_str = "AC AC 8C 9C KC"
        self.assertTrue(Hand(flush_str).has_flush)

    def test_has_full_house(self):
        full_house_str = "AC AD KS AS KC"
        self.assertTrue(Hand(full_house_str).has_full_house)

    def test_four_of_a_kind(self):
        four_str = "AC AD AS KH AH"
        self.assertTrue(Hand(four_str).has_four_of_a_kind)

    def test_has_straight_flush(self):
        straight_flush_str = "5C 6C 2C 3C 4C"
        self.assertTrue(Hand(straight_flush_str).has_straight_flush)

    def test_has_royal_flush(self):
        royal_flush_str = "TC JC KC QC AC"
        self.assertTrue(Hand(royal_flush_str).has_royal_flush)


class TestComparingHands(unittest.TestCase):
    """Test comparing hands"""

    def test_player_one_wins(self):
        p1_hand = Hand("TH TC TS AD 4C")
        p2_hand = Hand("AC AS 6D 3H 9D")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.THREE_OF_KIND, winning_hand_type)
        self.assertEqual(1, winner)
        self.assertEqual(p1_hand, winning_hand)

    def test_player_two_wins(self):
        # Royal flush, hand 2 wins
        p1_hand = Hand("TH TC TS AD 4C")
        p2_hand = Hand("AC KC QC JC TC")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.ROYAL_FLUSH, winning_hand_type)
        self.assertEqual(2, winner)
        self.assertEqual(p2_hand, winning_hand)

    def test_player_wins_with_high_card(self):
        # No hands, ace high, hand 1 wins
        p1_hand = Hand("3C 4H TS 8C AD")
        p2_hand = Hand("2C 7H JS 3C QD")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.HIGH_CARD, winning_hand_type)
        self.assertEqual(1, winner)
        self.assertEqual(p1_hand, winning_hand)

    def test_player_wins_with_fifth_high_card(self):
        # No hands, ace high, hand 1 wins
        p1_hand = Hand("AH TD 6D 4S 2D")
        p2_hand = Hand("AC TS 6C 4H 3D")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.HIGH_CARD, winning_hand_type)
        self.assertEqual(2, winner)
        self.assertEqual(p2_hand, winning_hand)

    def test_player_wins_after_pair_tie(self):
        # Tens high in pairs, hand 1 wins
        p1_hand = Hand("TH TC 7S AD 4C")
        p2_hand = Hand("9C 9H QC JC 3C")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.ONE_PAIR, winning_hand_type)
        self.assertEqual(1, winner)
        self.assertEqual(p1_hand, winning_hand)

    def test_player_wins_after_pair_value_tie(self):
        # Tens high in pairs, hand 1 wins
        p1_hand = Hand("TH TC 7S AD 4C")
        p2_hand = Hand("TD TS QC JC 3C")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.ONE_PAIR, winning_hand_type)
        self.assertEqual(1, winner)
        self.assertEqual(p1_hand, winning_hand)

    def test_player_wins_after_full_house_tie(self):
        # Queen high in threes, hand 2 wins
        p1_hand = Hand("TH TC 7S 7D 7C")
        p2_hand = Hand("9C 9H QC QC QC")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.FULL_HOUSE, winning_hand_type)
        self.assertEqual(2, winner)
        self.assertEqual(p2_hand, winning_hand)

    def test_player_wins_after_flush_tie(self):
        # Ace high, in a flush, hand 1 wins
        p1_hand = Hand("AC TC 7C 4C 2C")
        p2_hand = Hand("QH 9H JH 4H 2H")

        winner, winning_hand_type, winning_hand = compare_hands(p1_hand, p2_hand)
        self.assertEqual(HandType.FLUSH, winning_hand_type)
        self.assertEqual(1, winner)
        self.assertEqual(p1_hand, winning_hand)


class TestPassesEuler(unittest.TestCase):
    """Test wether we get the correct count of wins for player one"""

    def test_sample_input(self):
        p1 = Hand("5H 5C 6S 7S KD")
        p2 = Hand("2C 3S 8S 8D TD")
        winner, winning_hand_type, winning_hand = compare_hands(p1, p2)
        self.assertEqual(HandType.ONE_PAIR, winning_hand_type)
        self.assertEqual(2, winner)

        p1 = Hand("5D 8C 9S JS AC")
        p2 = Hand("2C 5C 7D 8S QH")
        winner, winning_hand_type, winning_hand = compare_hands(p1, p2)
        self.assertEqual(HandType.HIGH_CARD, winning_hand_type)
        self.assertEqual(1, winner)

        p1 = Hand("2D 9C AS AH AC")
        p2 = Hand("3D 6D 7D TD QD")
        winner, winning_hand_type, winning_hand = compare_hands(p1, p2)
        self.assertEqual(HandType.FLUSH, winning_hand_type)
        self.assertEqual(2, winner)

        p1 = Hand("4D 6S 9H QH QC")
        p2 = Hand("3D 6D 7H QD QS")
        winner, winning_hand_type, winning_hand = compare_hands(p1, p2)
        self.assertEqual(HandType.ONE_PAIR, winning_hand_type)
        self.assertEqual(1, winner)

        p1 = Hand("2H 2D 4C 4D 4S")
        p2 = Hand("3C 3D 3S 9S 9D")
        winner, winning_hand_type, winning_hand = compare_hands(p1, p2)
        self.assertEqual(HandType.FULL_HOUSE, winning_hand_type)
        self.assertEqual(1, winner)

    def test_poker_txt_input(self):
        player_one_wins = 0
        with open("poker.txt") as pokerFile:
            for deal in pokerFile:
                deal_list = deal.split(" ")
                player_one_hand = Hand(" ".join(deal_list[:5]))
                player_two_hand = Hand(" ".join(deal_list[5:]))
                w, ht, h = compare_hands(player_one_hand, player_two_hand)
                if w == 1:
                    player_one_wins += 1

        self.assertEqual(376, player_one_wins)
